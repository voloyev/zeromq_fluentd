require 'ffi-rzmq'
require 'net/http'
require 'socket'
require 'json'
require_relative 'common_methods'

class WorkersServer
  include CommonMethods

  attr_reader :dist_addr, :coll_addr, :num_workers
  
  def initialize(dist_addr, coll_addr, num_workers)
    @dist_addr   = dist_addr
    @coll_addr   = coll_addr
    @num_workers = num_workers
  end

  def run
    worker_threads = []

    num_workers.times do |worker_id|
      worker_threads = Thread.new do
        work(worker_id)
      end
    end

    worker_threads.join
  end

  def work(worker_id)
    puts "Started Worker #{worker_id}"
    dist_sock = ZCTX.socket(ZMQ::PULL)
    error_check(dist_sock.connect(dist_addr))

    coll_sock = ZCTX.socket(ZMQ::PUSH)
    error_check(coll_sock.connect(coll_addr))

    input_str = ''
    rc = 0
    while ZMQ::Util.resultcode_ok?(rc)
      rc = dist_sock.recv_string(input_str)
      error_check(rc)
      print '.'

      input = BERT.decode(input_str)
      url = input[:url]

      #Start building our output message
      output = { url: url, worker_id: worker_id }

      begin
        req_start = Time.now
        uri       = URI.parse(url)
        http_resp = nil
        Net::HTTP.start(uri.host, uri.port) do |http|
          http_resp = http.request_get(uri.path)
        end
        runtime   = Time.now - req_start
        output[:status]      = :success
        output[:runtime]     = runtime
        output[:http_code]   = http_resp.code.to_i
        puts output
      rescue StandardError => e
        output[:status]  = :error
        output[:message] = "P#{worker_id} Error: '#{e.message}'"
      end

      error_check(coll_sock.send_string(BERT.encode(output)))
    end
    
    error_check(dist_sock.close)
    error_check(coll_sock.close)
  end
end
