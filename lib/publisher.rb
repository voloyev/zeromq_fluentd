require 'ffi-rzmq'
require 'json'
require 'msgpack'

context = ZMQ::Context.new
publisher = context.socket(ZMQ::PUB)
publisher.bind('tcp://*:5563')

message_to_send = JSON.generate(
  {
    "key1" => "aaa", "key2" => "foo"
  })

loop do
  #publisher.send_string('test', ZMQ::SNDMORE)
  publisher.send_string(message_to_send)
  sleep 1
end

publisher.close
