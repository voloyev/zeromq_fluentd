require 'rubygems'
require 'ffi-rzmq'
require 'net/http'
require 'socket'
require 'json'
require_relative 'common_methods'

class Client
  include CommonMethods

  attr_reader :dist_addr, :coll_addr, :num_reqs, :urls
  
  def initialize(dist_addr, coll_addr, num_reqs, urls)
    @dist_addr = dist_addr
    @coll_addr = coll_addr
    @num_reqs  = num_reqs
    @urls      = urls
  end

  def run
    start_distributor
    start_collector

    ZCTX.terminate
  end

  private

  def start_distributor
    Thread.new do
      dist_sock = ZCTX.socket(ZMQ::PUSH)
      error_check(dist_sock.bind(@dist_addr))

      i = 0
      puts 'Distributor Started'
      while num_reqs.zero? || i < num_reqs
        i += 1

        message = { url: urls[rand(urls.length)] }
        error_check(dist_sock.send_string(BERT.encode(message)))

        print '-'
      end

      error_check(dist_sock.close)
    end
  end

  def start_collector
    coll_thread = Thread.new do
      coll_sock = ZCTX.socket(ZMQ::PULL)
      error_check(coll_sock.bind(@coll_addr))

      i = 0
      runtime_sum    = 0
      http_err_count = 0
      puts 'Collector Started'
      result_str = ''
      rc = 0
      results = {outputs: []}
      while (@num_reqs == 0 || i < @num_reqs) && ZMQ::Util.resultcode_ok?(rc)
        rc = coll_sock.recv_string(result_str)
        error_check(rc)
        puts result_str
        result = BERT.decode(result_str)
        results[:outputs] << result
        case result[:status]
        when :success
          print '.'
          runtime_sum += result[:runtime]
          if result[:http_code].to_i >= 400
            puts "HTTP Error: #{result.inspect}"
            http_err_count += 1
          end
        when :error
          $stderr.puts result.inspect
        end

        i += 1
      end

      send_to_fluent(results)
      puts "\nAll responses received."
      puts "AVG response time: #{runtime_sum / i.to_f}."
      puts "#{http_err_count} HTTP errors"

      error_check(coll_sock.close)
    end
    coll_thread.join
  end
end
