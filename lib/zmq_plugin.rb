require 'cztop'
require 'msgpack'

module ZmqPlugin
  class Server
    TCP_ADDR = 'tcp://127.0.0.1:4444'.freeze
    DEFAULT_TAG = 'testing'.freeze

    def initialize(args = {})
      @server = CZTop::Socket::PUSH.new(TCP_ADDR)
      @tag = args.fetch(:tag, DEFAULT_TAG)
    end

    def call(msg_body)
      msg = create_msg(msg_body).to_msgpack
      server << msg
      puts 'Sending msg'
      p msg
    rescue StandardError => error
      warn "write failed: #{$!} #{error}"
      false
    end

    private

    attr_reader :server, :tag

    def create_msg(msg_body = { 'key' => rand(1000) })
      [tag, Time.now.to_i, msg_body]
    end
  end
end
