require 'ffi-rzmq'
require 'msgpack'
require 'json'

context = ZMQ::Context.new
subscriber = context.socket(ZMQ::SUB)
subscriber.connect('tcp://localhost:5563')

subscriber.setsockopt(ZMQ::SUBSCRIBE, '')

loop do
  address = ''
  subscriber.recv_string(address)

  content = ''
  subscriber.recv_string(content)
  puts content
  puts address
end
