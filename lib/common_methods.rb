# frozen_string_literal: true

require './lib/zmq_plugin'

module CommonMethods
  def error_check(rc)
    if ZMQ::Util.resultcode_ok?(rc)
      false
    else
      STDERR.puts "Operation failed, \
      errno [#{ZMQ::Util.errno}] description [#{ZMQ::Util.error_string}]"
      caller(1).each { |callstack| STDERR.puts(callstack) }
      true
    end
  end

  def send_to_fluent(msg)
    ZmqPlugin::Server.new(tag: 'testing').call(msg)
    # json = JSON.generate(params)
    # uri = URI("http://localhost:8888/zeromq?json=#{json}")
    # Net::HTTP.get(uri)
  end
end
