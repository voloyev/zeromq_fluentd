require 'bert'
require 'optparse'
require './lib/workers_server'
require './lib/client'
require './lib/zmq_plugin'

ZCTX = ZMQ::Context.new(1)
trap('INT',
     proc do
       ZCTX.terminate
       exit
     end)

MODE = ARGV[0]

CONTROL_HOST = ARGV[1]
DISTR_ADDR   = "tcp://#{CONTROL_HOST}:2100".freeze
COLL_ADDR    = "tcp://#{CONTROL_HOST}:2101".freeze
NUM_REQS     = ARGV[2].to_i

def run_app
  case MODE
  when 'client'
    urls = ARGV[3..-1]
    Client.new(DISTR_ADDR, COLL_ADDR, NUM_REQS, urls).run
  when 'worker'
    num_workers = ARGV[2].to_i
    WorkersServer.new(DISTR_ADDR, COLL_ADDR, num_workers).run
  when 'test'
    server = ZmqPlugin::Server.new(tag: 'testing')

    loop do  
      server.call({ 'number' => rand(100).to_s })
      sleep(1)
    end
  end
end

run_app
