require './spec/spec_helper'
require './lib/zmq_plugin'

describe ZmqPlugin::Server do
  let(:msg) { {'key': 'value'} }

  it { expect(subject.call(msg)).to be_truthy }
end
